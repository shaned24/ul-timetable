package com.toughcrab.parsejson.app.popup;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.toughcrab.parsejson.app.Classes;
import com.toughcrab.parsejson.app.DBTools;
import com.toughcrab.parsejson.app.R;
import com.toughcrab.parsejson.app.modules.EditClassSlot;

import java.util.ArrayList;

public class ClassesOptionsPopup implements DialogInterface.OnClickListener{

    FragmentActivity fragmentActivity;
    View view;
    Classes selectedClass;

    public ClassesOptionsPopup(FragmentActivity activity, View view, Classes selectedClass) {
        this.fragmentActivity = activity;
        this.view = view;
        this.selectedClass = selectedClass;
    }

    public void showPopUp() {
        CharSequence options[] = new CharSequence[] {"Set Reminder", "Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(fragmentActivity);
        builder.setTitle("Options").setIcon(R.drawable.ic_action_edit);
        builder.setItems(options, this);
        builder.show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {

        Intent intent;

        switch (which) {
            case 0: //Set Reminder
                break;
            case 1: //Edit
                try {

                    intent = new Intent(fragmentActivity, EditClassSlot.class);

                    ArrayList<Classes> myClasses = new ArrayList<Classes>();
                    myClasses.add(selectedClass);

                    intent.putExtra("selectedClass", myClasses);
                    intent.putParcelableArrayListExtra("selectedClass", myClasses);
                    //Start the edit activity with the selected class data
                    fragmentActivity.startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case 2: //Delete
                DBTools dbTools = new DBTools(fragmentActivity);
                break;
        }

    }

    /*@Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setAlarm :
                Toast.makeText(fragmentActivity, "You selected the action : " + item.getTitle(), Toast.LENGTH_SHORT).show();
                break;
            case R.id.editClass :
                Toast.makeText(fragmentActivity, "You selected the action : " + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
            case  R.id.deleteClass :
                Toast.makeText(fragmentActivity, "You selected the action : " + item.getTitle(), Toast.LENGTH_SHORT).show();

                break;
        }
        return true;
    }*/



}

package com.toughcrab.parsejson.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class AddClass extends ActionBarActivity implements View.OnClickListener {

    TextView day, startTime, endTime, module, modulename, type, room;
    Button submitClass;
    TextView[] all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);
        setTitle("Add New Class");

        all = new TextView[7];

        all[0] = day = (TextView)findViewById(R.id.day);
        all[1] = startTime = (TextView)findViewById(R.id.startTime);
        all[2] = endTime = (TextView)findViewById(R.id.endTime);
        all[3] = module = (TextView)findViewById(R.id.module);
        all[4] = modulename = (TextView)findViewById(R.id.modulename);
        all[5] = type = (TextView)findViewById(R.id.type);
        all[6] = room = (TextView)findViewById(R.id.room);

        submitClass = (Button)findViewById(R.id.submitClass);
        submitClass.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        for(int i = 0; i<all.length;i++) {
            if(all[i].getText().length() < 1) {
                Context context = getApplicationContext();
                CharSequence text = "The fields Must Contain Data!";
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                return;
            }
        }
        new AlertDialog.Builder(this)
                .setTitle("Confirm")
                .setMessage("By accepting you will add this new class to your timetable.")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        processClassCreation();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .show();

    }

    public void processClassCreation() {

        Classes myClass = new Classes();

        myClass.setDay(day.getText().toString());
        myClass.setStart(startTime.getText().toString());
        myClass.setEnd(endTime.getText().toString());
        myClass.setModule(module.getText().toString());
        myClass.setModulename(modulename.getText().toString());
        myClass.setType(type.getText().toString());
        myClass.setRoom(room.getText().toString());
        myClass.setLength("1");
        myClass.setGroup("1");

        DBTools db = new DBTools(this);
        db.insertClass(myClass);

        Context context = getApplicationContext();
        CharSequence text = "Class Added!";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent(this, Timetable.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}





package com.toughcrab.parsejson.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created by Shane on 4/6/2014.
 */
public abstract class FragmentActivityBuilder extends FragmentActivity {

    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        FragmentManager fragManager = getSupportFragmentManager();
//        // Check if the FragmentManager knows about the Fragment
//        // id we refer to
//        Fragment theFragment = fragManager.findFragmentById(R.id.timetableFragmentContainer);
//        // Check if the Fragment was found
//        if(theFragment == null){
//            theFragment = createFragment();
//            fragManager.beginTransaction()
//                    .add(R.id.timetableFragmentContainer, theFragment)
//                    .commit();
//        }

    }
}

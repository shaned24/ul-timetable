package com.toughcrab.parsejson.app;

/**
 * Created by Shane on 4/4/2014.
 */
public class Day {

    String day;
    String start;
    String end;
    String length;
    String module;
    String modulename;
    String type;
    String group;
    String room;

    public String getDay() {
        return day;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getLength() {
        return length;
    }

    public String getModule() {
        return module;
    }

    public String getModulename() {
        return modulename;
    }

    public String getType() {
        return type;
    }

    public String getGroup() {
        return group;
    }

    public String getRoom() {
        return room;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setRoom(String room) {
        this.room = room;
    }
}

package com.toughcrab.parsejson.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;

import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;


public class Timetable extends ActionBarActivity implements OnTimetableReceived {

    private ViewPager viewPager;
    private TimetableViewPager timetableViewPager;
    private String[] days;
    public ArrayList<Classes> allClasses;
    public DBTools dbTools = new DBTools(this);
    public static Activity timetable;
    public ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        timetable = this;

        dialog = new ProgressDialog(this);

        viewPager = new ViewPager(this);

        viewPager.setId(R.id.viewPager);

        setContentView(viewPager);

        initTimetableActivity();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void initTimetableActivity() {


        int countData = dbTools.getAllClasses().size();

        if(countData == 0) {
            dialog.setMessage("Loading your timetable...");
            dialog.show();

            new TimetableData(this).execute();
        } else {
            initialiseView();
        }
    }

    @Override
    public void onTimetableReceived(String result) {
        //parse JSON to an ArrayList of type Classes
        ArrayList<Classes> allClasses = (ArrayList<Classes>) new JSONParser().parseJSON(result);
        //clear the db for the new user

        if(allClasses.isEmpty())
        {
            Context context = getApplicationContext();
            CharSequence text = getString(R.string.cannot_fetch_timetable);
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
            // Do something with the empty list here.
        } else {
            for(int i = 0;i<allClasses.size(); i++) {
                //insert each Class into the db
                dbTools.insertClass(allClasses.get(i));
            }
            initialiseView();
        }

    }

    public void initialiseView() {
        dialog.dismiss();

        //should get from db here
        this.allClasses = dbTools.getAllClasses();

        //init the String array with the days of the week
        days = new String[5];
        days[0] = "Monday";
        days[1] = "Tuesday";
        days[2] = "Wednesday";
        days[3] = "Thursday";
        days[4] = "Friday";

        //instantiate the TimetableViewPager class by passing the context and the view pager container from the xml layout
        timetableViewPager = new TimetableViewPager(this, viewPager);

        //get the current action bar, this is so we can add things like tabs to it later
        ActionBar bar = getActionBar();

        //loop over all of the days
        for(int j = 0; j<days.length;j++) {


            ArrayList<Classes> temp = new ArrayList<Classes>();

            //loop over the arraylist of Classes
            for(int i = 0;i<allClasses.size(); i++) {

                //check if the day matches the day that is set in the current Classes object
                if(allClasses.get(i).getDay().equals(days[j])) {
                    //if so add that class to a temporary ArrayList<Classes>
                    temp.add(allClasses.get(i));
                }
            }
            //create a new bundle object, this will be used to pass data to the timetable fragment
            Bundle tempBundle = new Bundle();
            //Android Parcelable implementation allows objects to read and write from Parcels which can contain flattened data inside message containers.
            //so we can pass the ArrayList of Classes objects to the fragment TimetableFragment
            tempBundle.putParcelableArrayList("data", temp);

            //adda tab to the viewpager with the name of the day as the heading
            //pass the dynamically created fragment and the arugments which is the bundle of ArrayList<Classes>
            //the TimetableViewPager takes care of instantiating the dynamic TimetableFragment with the arguments it has been given
            timetableViewPager.addTab(bar.newTab().setText(days[j]), TimetableFragment.class, tempBundle);
        }

        //Set the navigation mode of the tabs to a swipe, so the user can seamlessly swipe through the tabs
        bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        //this method finds what day it is and sets the default selected tab to the current day
        setDay();

    }

    private void setDay() {
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        viewPager.setCurrentItem(dayOfWeek - 2);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.timetable, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SetupActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}

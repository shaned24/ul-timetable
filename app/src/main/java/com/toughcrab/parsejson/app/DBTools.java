package com.toughcrab.parsejson.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Shane on 4/7/2014.
 */
public class DBTools extends SQLiteOpenHelper {

    public DBTools(Context context) {
        super(context, "timetable_db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String query = "CREATE TABLE timetable " +
                "(id INTEGER PRIMARY KEY, " +
                "day TEXT," +
                "start TEXT," +
                "end TEXT," +
                "length TEXT," +
                "module TEXT," +
                "modulename TEXT," +
                "type TEXT," +
                "classGroup TEXT," +
                "room TEXT)";

        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String query = "DROP TABLE IF EXISTS timetable";

        db.execSQL(query);

        onCreate(db);
    }

    public void newUser() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DROP TABLE IF EXISTS timetable";
        db.execSQL(query);
        onCreate(db);
    }

    public void insertClass(Classes myClass) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put("day" , myClass.getDay());
        contentValues.put("start" , myClass.getStart());
        contentValues.put("end" , myClass.getEnd());
        contentValues.put("length" , myClass.getLength());
        contentValues.put("module" , myClass.getModule());
        contentValues.put("modulename" , myClass.getModulename());
        contentValues.put("type" , myClass.getType());
        contentValues.put("classGroup", myClass.getGroup());
        contentValues.put("room" , myClass.getRoom());

        db.insert("timetable", null, contentValues);

        db.close();
    }

    public int updateTimetable(Classes myClass) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put("day" , myClass.getDay());
        contentValues.put("start" , myClass.getStart());
        contentValues.put("end" , myClass.getEnd());
        contentValues.put("length" , myClass.getLength());
        contentValues.put("module" , myClass.getModule());
        contentValues.put("modulename" , myClass.getModulename());
        contentValues.put("type" , myClass.getType());
        contentValues.put("classGroup" , myClass.getGroup());
        contentValues.put("room" , myClass.getRoom());

        return db.update("timetable",
                contentValues,
                "day = ? AND start = ? AND end = ? AND module = ?",
                new String[] {myClass.getDay(), myClass.getStart(), myClass.getEnd(), myClass.getModule()}
        );

    }

    public void deleteClass(Classes myClass) {

        SQLiteDatabase db = this.getWritableDatabase();

        String query = "DELETE FROM timetable WHERE day =' "+myClass.getDay()+"' AND start ='"+myClass.getStart()+"' AND end ='"+myClass.getEnd()+"' AND module = '"+myClass.getModule()+"'";

        db.execSQL(query);
    }

    public ArrayList<Classes> getAllClasses(){

        ArrayList<Classes> allClasses = new ArrayList<Classes>();

        SQLiteDatabase db = this.getWritableDatabase();

        String query = "SELECT * FROM timetable";

        Cursor cursor = db.rawQuery(query, null);

        if(cursor.moveToFirst()) {

            do {
                Classes myClass = new Classes();

                myClass.setId(cursor.getString(0));
                myClass.setDay(cursor.getString(1));
                myClass.setStart(cursor.getString(2));
                myClass.setEnd(cursor.getString(3));
                myClass.setLength(cursor.getString(4));
                myClass.setModule(cursor.getString(5));
                myClass.setModulename(cursor.getString(6));
                myClass.setType(cursor.getString(7));
                myClass.setGroup(cursor.getString(8));
                myClass.setRoom(cursor.getString(9));

                allClasses.add(myClass);

            } while (cursor.moveToNext());
        }

        return  allClasses;
    }

    public Classes getClass(String id) {

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT * FROM contacts WHERE id ='" + id + "'";

        Cursor cursor = db.rawQuery(query, null);

        Classes myClass = new Classes();

        if(cursor.moveToFirst()) {

            do {

                myClass.setId(cursor.getString(0));
                myClass.setDay(cursor.getString(1));
                myClass.setStart(cursor.getString(2));
                myClass.setEnd(cursor.getString(3));
                myClass.setLength(cursor.getString(4));
                myClass.setModule(cursor.getString(5));
                myClass.setModulename(cursor.getString(6));
                myClass.setType(cursor.getString(7));
                myClass.setGroup(cursor.getString(8));
                myClass.setRoom(cursor.getString(9));

            } while (cursor.moveToNext());
        }

        return myClass;
    }
}

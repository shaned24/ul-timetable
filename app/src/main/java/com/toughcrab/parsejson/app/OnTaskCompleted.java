package com.toughcrab.parsejson.app;

import java.util.ArrayList;

/**
 * Created by Shane on 4/6/2014.
 */
interface OnTimetableReceived {

    void onTimetableReceived(String result);

}

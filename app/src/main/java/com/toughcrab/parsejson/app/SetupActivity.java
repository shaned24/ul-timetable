package com.toughcrab.parsejson.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SetupActivity extends ActionBarActivity implements View.OnClickListener {

    EditText studentIdeditText;
    Button submitStudentIdButton;
    DBTools dbTools = new DBTools(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);
        submitStudentIdButton = (Button)findViewById(R.id.submitStudentIdButton);
        submitStudentIdButton.setOnClickListener(this);
        studentIdeditText = (EditText)findViewById(R.id.studentIdeditText);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submitStudentIdButton:

                if(studentIdeditText.getText().equals("") || studentIdeditText.getText().length() <8 ) {
                    Context context = getApplicationContext();
                    CharSequence text = "Please enter a valid student ID.";
                    int duration = Toast.LENGTH_SHORT;
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    break;
                }

                new AlertDialog.Builder(this)
                        .setTitle("Are you sure?")
                        .setMessage("By accepting you will delete any previous timetable data.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                saveStudentID(Integer.parseInt(String.valueOf(studentIdeditText.getText())));
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(R.drawable.abc_ic_go)
                        .show();

            break;
        }

    }

    public void saveStudentID(int studentId) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putInt("STUDENTID", studentId);
        edit.commit();

        if(Timetable.timetable != null) {
            Timetable.timetable.finish();
        }

        dbTools.newUser();

        Context context = getApplicationContext();

        CharSequence text = "New student ID saved.";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        this.startActivity(intent);
    }

    public int loadStudentID() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        int studentId = sharedPreferences.getInt("STUDENTID", 0);
        return studentId;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}





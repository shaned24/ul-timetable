package com.toughcrab.parsejson.app;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shane on 4/4/2014.
 */
public class Classes implements Parcelable {

    String id;
    String day;
    String start;
    String end;
    String length;
    String module;
    String modulename;
    String type;
    String group;
    String room;

    public Classes() {

    }

    public String getId() { return id; }

    public String getDay() {
        return day;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getLength() {
        return length;
    }

    public String getModule() {
        return module;
    }

    public String getModulename() {
        return modulename;
    }

    public String getType() {
        return type;
    }

    public String getGroup() {
        return group;
    }

    public String getRoom() {
        return room;
    }

    public void setId(String id) { this.id = id; }

    public void setDay(String day) {
        this.day = day;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public void setModulename(String modulename) {
        this.modulename = modulename;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(day);
        dest.writeString(start);
        dest.writeString(end);
        dest.writeString(length);
        dest.writeString(module);
        dest.writeString(modulename);
        dest.writeString(type);
        dest.writeString(group);
        dest.writeString(room);
    }

    public static final Parcelable.Creator<Classes> CREATOR
            = new Parcelable.Creator<Classes>() {
        public Classes createFromParcel(Parcel in) {
            return new Classes(in);
        }

        public Classes[] newArray(int size) {
            return new Classes[size];
        }
    };

    public Classes(Parcel in) {
        id = in.readString();
        day = in.readString();
        start = in.readString();
        end = in.readString();
        length = in.readString();
        module = in.readString();
        modulename = in.readString();
        type = in.readString();
        group = in.readString();
        room = in.readString();
    }

    @Override
    public String toString() {
        return this.day + " at " + this.start + " for " + this.module + " " + this.modulename + " it finishes at " + this.end;
    }
}

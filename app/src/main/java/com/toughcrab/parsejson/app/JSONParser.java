package com.toughcrab.parsejson.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shane on 4/4/2014.
 */
public class JSONParser {
    String[] days;

    JSONParser() {
        days = new String[5];
        days[0] = "Monday";
        days[1] = "Tuesday";
        days[2] = "Wednesday";
        days[3] = "Thursday";
        days[4] = "Friday";
    }

    public List<Classes> parseJSON(String result) {

        JSONObject jsonObject;
        JSONObject queryJSONObject = null;

        try{
            jsonObject = new JSONObject(result);
            //get the timetable object
            queryJSONObject = jsonObject.getJSONObject("timetable");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayList<Classes> allClasses = new ArrayList<Classes>();

        for(int j = 0; j<days.length; j++ ) {
            try {
                //Get the JSON object corresponding to the property passed in
                JSONArray JSONDay;
                JSONDay = queryJSONObject.getJSONArray(days[j]);

                for (int i = 0; i < JSONDay.length(); i++) {
                    //we have to deal with the Array of Monday object one at a time
                    JSONObject currentObject = JSONDay.getJSONObject(i);
                    //classes  object
                    Classes classes = new Classes();

                    String day = currentObject.getString("day").toString();
                    classes.setDay(day);

                    String start = currentObject.getString("start").toString();
                    classes.setStart(start);

                    String end = currentObject.getString("end").toString();
                    classes.setEnd(end);

                    String length = currentObject.getString("length").toString();
                    classes.setLength(length);

                    String module = currentObject.getString("module").toString();
                    classes.setModule(module);

                    String modulename = currentObject.getString("modulename").toString();
                    classes.setModulename(modulename);

                    String type = currentObject.getString("type").toString();
                    classes.setType(type);

                    String group = currentObject.getString("group").toString();
                    classes.setGroup(group);

                    String room = currentObject.getString("room").toString();
                    classes.setRoom(room);
                    //then we can get the properties of each object and use the data that they reference
                    allClasses.add(classes);
                }

            } catch (JSONException e) {
                e.printStackTrace();

            }
            //catch if there result has nothing in it, this could be invalid id numbers etc
            catch(NullPointerException e) {
                return new ArrayList<Classes>();
            }
        }

        return allClasses;
    }
}

package com.toughcrab.parsejson.app;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Shane on 4/6/2014.
 */

public class TimetableData extends AsyncTask<String, String, String> {

    private OnTimetableReceived callback;

    public TimetableData(OnTimetableReceived callback) {
        this.callback = callback;
    }

    @Override
    protected String doInBackground(String... params) {

        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(MainActivity.timetable_url + String.valueOf(MainActivity.studentID));
        httppost.setHeader("Content-type", "application/json");
        InputStream inputStream = null;
        String result = null;

        try{

            HttpResponse response = httpClient.execute(httppost);
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8 );
            StringBuilder theStringBuilder = new StringBuilder();
            String line = null;

            while((line = reader.readLine()) != null){
                theStringBuilder.append(line + "\n");
            }

            result = theStringBuilder.toString();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            try{
                if(inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {


        //call the callback so that the activity using it can do something with this new profound data!
        callback.onTimetableReceived(result);

    }
}


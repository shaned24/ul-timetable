package com.toughcrab.parsejson.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.toughcrab.parsejson.app.popup.ClassesOptionsPopup;

import java.util.ArrayList;

public class TimetableFragment extends ListFragment {

    ArrayList<Classes> allClasses;
    public String[] colors = new String[3];
    public String[] types = new String[3];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        colors[0] = "#2d6ca2";
        colors[1] = "#419641";
        colors[2] = "#eb9316";

        types[0] = "LEC";
        types[1] = "TUT";
        types[2] = "LAB";

        allClasses = getArguments().getParcelableArrayList("data");
        ClassesAdapter adapter = new ClassesAdapter(allClasses);
        setListAdapter(adapter);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AdapterView.OnItemLongClickListener listener = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
//                Toast.makeText(getActivity(), "Item in " + allClasses.get(pos).getId() + " was clicked", Toast.LENGTH_SHORT).show();
                //want the popup menu to appear here

                //Introduce the fragment here
                //it will be a popup menu


                /*ClassesOptionsPopup classesPopup = new ClassesOptionsPopup(getActivity(), arg1, allClasses.get(pos));
                classesPopup.showPopUp();*/

                return false;
            }
        };

        ListView myList = getListView();
        myList.setOnItemLongClickListener(listener);

    }

    private class ClassesAdapter extends ArrayAdapter<Classes> {

        public ClassesAdapter(ArrayList<Classes> allClasses) {
            super(getActivity(), android.R.layout.simple_list_item_1, allClasses);
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null) {

                convertView = getActivity().getLayoutInflater().inflate(R.layout.timetable_data, null);
            }

            Classes theClass = getItem(position);

            TextView startTime = (TextView) convertView.findViewById(R.id.startTime);
            startTime.setText(theClass.getStart());

            TextView endTime = (TextView) convertView.findViewById(R.id.endTime);
            endTime.setText(theClass.getEnd());

            TextView module = (TextView) convertView.findViewById(R.id.module);
            module.setText(theClass.getModule());

            TextView modulename = (TextView) convertView.findViewById(R.id.modulename);
            modulename.setText(theClass.getModulename());

            TextView room = (TextView) convertView.findViewById(R.id.room);

            if(theClass.getRoom().equals("&nbsp;")) {
                String theRoom = "Not Available";
                room.setText(theRoom);
                room.setTextColor(Color.parseColor("#C12E2A"));
            } else {
                room.setText(theClass.getRoom());
            }

            TextView type = (TextView) convertView.findViewById(R.id.type);
            type.setText(theClass.getType());

            String selectedColor = "#000000";
            for(int i = 0; i<types.length; i++) {
                if(types[i].equals(theClass.getType()))
                {
                    selectedColor = colors[i];
                }
            }

            type.setTextColor(Color.parseColor(selectedColor));

            return convertView;
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int pos, long id) {
        super.onListItemClick(l, v, pos, id);
//        Toast.makeText(getActivity(), "Item in " + allClasses.get(pos).getId() + " was clicked", Toast.LENGTH_SHORT).show();
        ClassesOptionsPopup classesPopup = new ClassesOptionsPopup(getActivity(), v, allClasses.get(pos));
        classesPopup.showPopUp();
    }



}

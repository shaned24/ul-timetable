package com.toughcrab.parsejson.app.modules;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.toughcrab.parsejson.app.Classes;
import com.toughcrab.parsejson.app.R;

import java.util.ArrayList;

public class EditClassSlot extends ActionBarActivity {
    Classes selectedClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        ArrayList<Classes> myClass = intent.getParcelableArrayListExtra("selectedClass");
        selectedClass = myClass.get(0);
        initializeView();
    }

    private void initializeView() {

        setContentView(R.layout.activity_edit_class_slot);
        setTitle("Edit");

        String startTimeOLD = selectedClass.getStart();
        String endTimeOLD = selectedClass.getEnd();
        String dayOLD = selectedClass.getDay();

        //startTime
        String[] startTimes = new String[9];

        startTimes[0]="9:00";
        startTimes[1]="10:00";
        startTimes[2]="11:00";
        startTimes[3]="12:00";
        startTimes[4]="13:00";
        startTimes[5]="14:00";
        startTimes[6]="15:00";
        startTimes[7]="16:00";
        startTimes[8]="17:00";
        /*Spinner startTimeSpinner = (Spinner) findViewById(R.id.startTimeSpinner);
        Spinner endTimeSpinner = (Spinner) findViewById(R.id.endTimeSpinner);

        ArrayAdapter timeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, startTimes);

        startTimeSpinner.setAdapter(timeAdapter);
        endTimeSpinner.setAdapter(timeAdapter);

        int currentStartTime = timeAdapter.getPosition(selectedClass.getStart());
        int currentEndTime = timeAdapter.getPosition(selectedClass.getEnd());

        startTimeSpinner.setSelection(currentStartTime);
        endTimeSpinner.setSelection(currentEndTime);*/

        /*String myString = "some value"; //the value you want the position for

        ArrayAdapter myAdap = (ArrayAdapter) mySpinner.getAdapter(); //cast to an ArrayAdapter

        int spinnerPosition = myAdap.getPosition(myString);

        //set the default according to value
        mySpinner.setSelection(spinnerPosition);*/
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}

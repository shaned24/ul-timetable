package com.toughcrab.parsejson.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    static String timetable_url = "http://shanedaly.net/timetable/getAndroidTimetable/";
    static int studentID = 0;

    TextView studentIDTextView;
    Button viewTimetable, addClass, viewSettings, resetTimetable;
    DBTools db = new DBTools(this);

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        studentID = sharedPreferences.getInt("STUDENTID", 0);

        if(studentID == 0) {
            Intent intent = new Intent(MainActivity.this, SetupActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }

        studentIDTextView = (TextView)findViewById(R.id.studentIDTextView);
        studentIDTextView.setText(String.valueOf(studentID));

        viewTimetable = (Button)findViewById(R.id.viewTimetable);
        viewTimetable.setOnClickListener(this);

        addClass = (Button)findViewById(R.id.addClass);
        addClass.setOnClickListener(this);

        viewSettings = (Button)findViewById(R.id.viewSettings);
        viewSettings.setOnClickListener(this);

        resetTimetable = (Button)findViewById(R.id.resetTimetable);
        resetTimetable.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        studentID = sharedPreferences.getInt("STUDENTID", 0);
        studentIDTextView.setText(String.valueOf(studentID));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, MainActivity.class);

            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
//            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.viewTimetable:
                intent = new Intent(MainActivity.this, Timetable.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            break;
            case R.id.viewSettings:
                intent = new Intent(this, SetupActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            break;
            case R.id.addClass:
                intent = new Intent(this, AddClass.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                break;
            case R.id.resetTimetable:
                new AlertDialog.Builder(this)
                        .setTitle("Confirm")
                        .setMessage("By accepting you will erase existing timetable data and a new timetable will have to be fetched.")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                db.newUser();
                                Context context = getApplicationContext();
                                CharSequence text = "timetable has been reset!";
                                int duration = Toast.LENGTH_SHORT;
                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .show();
                break;

            default:
            break;

        }
    }
}
